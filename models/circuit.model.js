const mongoose = require("mongoose");
const CircuitSchema = new mongoose.Schema({
  titre :{type: String,trim: true,required:'Titre is required' },
  active:{type: Boolean, default:true},
  points:[{type:String}]
})




const Circuit = mongoose.model('Circuit', CircuitSchema);
module.exports = Circuit;