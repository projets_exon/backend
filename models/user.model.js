const mongoose = require("mongoose");
const crypto = require('crypto');
const UserSchema = new mongoose.Schema({
  pseudo :{type: String,unique:true,trim: true,required:'Pseudo is required' },
  firstName: {type: String,trim: true,required: 'Firstname is required'},
  lastName: {type: String,trim: true,required: 'Lastname is required'},
  active:{type: Boolean, default:true},
  language:{type: String, default:"default"},
  created: {type: Date,default: Date.now},
  role: {type: String,enum : ["AD","J"],default:"J"},
  code: {type: String,default: ""},


  hashed_password: {
    type: String,
  },
  salt: String,
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  }
})

UserSchema
  .virtual('password')
  .set(function (password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function () {
    return this._password
  })

UserSchema.path('hashed_password').validate(function (v) {
  if (this._password && this._password.length < 6) {
    this.invalidate('password', 'Password must be at least 6 characters.')
  }
  if (this.isNew && !this._password) {
    this.invalidate('password', 'Password is required')
  }
}, null)

UserSchema.methods = {
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password
  },
  encryptPassword: function (password) {
    if (!password) return ''
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex')
    } catch (err) {
      return ''
    }
  },
  makeSalt: function () {
    return Math.round((new Date().valueOf() * Math.random())) + ''
  }
}


const User = mongoose.model('User', UserSchema);
module.exports = User;