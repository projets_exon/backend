const mongoose = require("mongoose");
const PlaySchema = new mongoose.Schema({
  userId:{type:String,trim:true,required:'User ID is required'},  
  circuitId:{type:String,trim:true,required:'Circuit ID is required'},  
  joue:[{type:String}],
  indice:{type:Number,default:1}
})




const Play = mongoose.model('Play', PlaySchema);
module.exports = Play;