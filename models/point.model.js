const mongoose = require("mongoose");
const PointSchema = new mongoose.Schema({
  titre :{type: String,trim: true,required:'Titre is required' },
  description :{type: String,trim: true,required:'description is required' },
  longitude :{type: String,trim: true,required:'longitude is required' },
  latitude :{type: String,trim: true,required:'latitude is required' },
  video :{type: String,trim: true,required:'video is required' },
  image :{type: String,trim: true,required:'image is required' },
  active:{type: Boolean, default:true},

})




const Point = mongoose.model('Point', PointSchema);
module.exports = Point;