const express = require("express");
const pointCtrl = require('../controllers/point.controller');
const multipart = require('connect-multiparty');
const router = express.Router();

const multipartMiddleware = multipart({
  uploadDir: 'uploads'
});
router.route('/api/points')
  .get(pointCtrl.list)
  .post(multipartMiddleware,pointCtrl.create)
router.route('/api/point/:pointId')
  .get(pointCtrl.pt)  
  .put(pointCtrl.update)
  .delete(pointCtrl.remove)
router.param('pointId', pointCtrl.pointByID)

module.exports = router;