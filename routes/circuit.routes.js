const express = require("express");
const circuitCtrl = require('../controllers/circuit.controller');

const router = express.Router()

router.route('/api/circuit')
  .get(circuitCtrl.list)
  .post(circuitCtrl.create)
router.route('/api/circuit/:crId')
  .get(circuitCtrl.cr)
  .put(circuitCtrl.update)
  .delete(circuitCtrl.remove)
router.param('crId', circuitCtrl.circuitByID)

module.exports = router;