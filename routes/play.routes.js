const express = require("express");
const playCtrl = require('../controllers/play.controller');

const router = express.Router()

router.route('/api/play')
  .get(playCtrl.list)
  .post(playCtrl.create)
router.route('/api/play/:plId')
  .get(playCtrl.pl)
router.route('/api/plays/:userId')
  .get(playCtrl.playsByUser)
router.param('plId', playCtrl.playByID)

module.exports = router;