const express = require("express");
const userCtrl = require('../controllers/user.controller');

const router = express.Router()

router.route('/api/users')
  .get(userCtrl.list)
  .post(userCtrl.create)
router.route('/api/user/:userId')
  .get(userCtrl.us)
router.param('userId', userCtrl.userByID)

module.exports = router;