require("dotenv").config();
const express = require("express");
const cookieParser = require('cookie-parser');
const compress = require('compression');
const helmet = require("helmet");
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(cookieParser())
app.use(compress())
// secure apps by setting various HTTP headers
app.use(helmet())
// enable CORS - Cross Origin Resource Sharing
app.use(cors())
//app.use(cors({origin: 'https://test.tradr.wijaa-technologies.com'}))// express routing
app.use(express.static("public"));
// routes
const userRoutes = require("./routes/user.routes");
const authRoutes = require("./routes/auth.routes");
const circuitRoutes = require("./routes/circuit.routes");
const pointRoutes = require("./routes/point.routes");
const playRoutes = require("./routes/play.routes");

app.use('/uploads', express.static('files'));
app.get('/api/uploads/:name', function (req, res, next) {
  const fileName = req.params.name;
  res.sendFile(fileName, { root: './uploads' })
});

// mount routes
app.use('/', userRoutes)
app.use('/', authRoutes)
app.use('/', circuitRoutes)
app.use('/', pointRoutes)
app.use('/', playRoutes)

// Catch unauthorised errors
app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({"error" : err.name + ": " + err.message})
  }else if (err) {
    res.status(400).json({"error" : err.name + ": " + err.message})
    console.log(err)
  }
})



module.exports=app;
