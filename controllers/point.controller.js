const Point = require('../models/point.model');
const errorHandler = require('./../helpers/dbErrorHandler');

const create = async (req, res) => {
    try {
        let point = new Point(req.body)
        point.video=req.files.video.path.split('\\')[1];
        point.image=req.files.image.path.split('\\')[1];
        let result = await point.save()
        res.status(200).json(result)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}

/**
 * Load pointByID .
 */
const pointByID = async (req, res, next, id) => {
    try {
        let point = await Point.findById(id)
        if (!point)
            return res.status('400').json({
                error: "Point not found"
            })
        req.point = point
        next()
    } catch (err) {
        return res.status('400').json({
            error: "Could not retrieve point"
        })
    }
}
const pt = async (req, res) => {
    console.log(req.point)
}

const list = async (req, res) => {
    try {
        let points = await Point.find()
        res.json(points)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}



const remove = async (req, res) => {
    try {
        let point = req.point
        let deletedPoint = await point.remove()
        res.json(deletedPoint)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}



const update = async (req, res) => {
    try{
        if (!req.point)
            return res.status('400').json({
                error: "Point not found"
            })
    let point = await Point.findByIdAndUpdate(req.point._id,req.body, {new: true})
    return res.status('201').json(point)}
    catch{
        return res.status('400').json({
            error: "Could not retrieve point"
        })
    }
}








module.exports = {
    create,
    pointByID,pt,
    list,
    remove,
    update
}
