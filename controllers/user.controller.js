const User = require('../models/user.model');
const errorHandler = require('./../helpers/dbErrorHandler');

const create = async (req, res) => {
  try {
    let user = new User(req.body)
    let result = await user.save()
    res.status(200).json(result)
  } catch (err){
    return res.status(400).json({
      error: errorHandler.getErrorMessage(err)
    })
  }
}

/**
 * Load user and append to req.
 */
const userByID = async (req, res, next, id) => {
  try {
    let user = await User.findById(id)
    if (!user)
      return res.status('400').json({
        error: "User not found"
      })
    req.profile = user
    next()
  } catch (err) {
    return res.status('400').json({
      error: "Could not retrieve user"
    })
  }
}




const list = async (req, res) => {
  try {
    let users = await User.find().select('_id pseudo firstName lastName created')
    res.json(users)
  } catch (err) {
    return res.status(400).json({
      error: errorHandler.getErrorMessage(err)
    })
  }
}



const remove = async (req, res) => {
  try {
    let user = req.profile
    let deletedUser = await user.remove()
    deletedUser.hashed_password = undefined
    deletedUser.salt = undefined
    res.json(deletedUser)
  } catch (err) {
    return res.status(400).json({
      error: errorHandler.getErrorMessage(err)
    })
  }
}


const us = async (req, res) => {
 console.log(req.profile)
}









module.exports = {
  create,
  userByID,
  list,
  remove,
  us
}
