const Circuit = require('../models/circuit.model');
const errorHandler = require('./../helpers/dbErrorHandler');

const create = async (req, res) => {
    try {
        let circuit = new Circuit(req.body)
        let result = await circuit.save()
        res.status(200).json(result)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}

/**
 * Load circuitByID .
 */
const circuitByID = async (req, res, next, id) => {
    try {
        let circuit = await Circuit.findById(id)
        if (!circuit)
            return res.status('400').json({
                error: "Circuit not found"
            })
        req.circuit = circuit
        next()
    } catch (err) {
        return res.status('400').json({
            error: "Could not retrieve circuit"
        })
    }
}
const cr = async (req, res) => {
    console.log(req.circuit)
}
const update = async (req, res) => {
    try{
        if (!req.circuit)
            return res.status('400').json({
                error: "Circuit not found"
            })
    let circuit = await Circuit.findByIdAndUpdate(req.circuit._id,req.body, {new: true})
    return res.status('201').json(circuit)}
    catch{
        return res.status('400').json({
            error: "Could not retrieve circuit"
        })
    }
}

const list = async (req, res) => {
    try {
        let circuits = await Circuit.find().select('_id titre active')
        res.json(circuits)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}



const remove = async (req, res) => {
    try {
        let circuit = req.circuit
        let deletedCircuit = await circuit.remove()
        return res.status('203').json(deletedCircuit)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}












module.exports = {
    create,
    circuitByID,cr,
    list,
    remove,
    update
}
