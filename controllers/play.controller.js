const Play = require('../models/Play.model');
const errorHandler = require('./../helpers/dbErrorHandler');

const create = async (req, res) => {
    try {
        let play = new Play(req.body)
        let result = await play.save()
        res.status(200).json(result)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}

/**
 * Load playByID .
 */
const playByID = async (req, res, next, id) => {
    try {
        let play = await Play.findById(id)
        if (!play)
            return res.status('400').json({
                error: "Play not found"
            })
        req.play = play
        next()
    } catch (err) {
        return res.status('400').json({
            error: "Could not retrieve Play"
        })
    }
}
const playsByUser = async (req, res) => {
    try {
        let play = await Play.find({'userId':req.params.userId})
        if (play)
        res.status('200').json(play)    
        else
         res.status('400').json({
            error: "Play not found"
        })
    } catch (err) {
        return res.status('400').json({
            error: "Could not retrieve Plays"
        })
    }
}
const pl = async (req, res) => {
    console.log(req.play)
}

const list = async (req, res) => {
    try {
        let plays = await Play.find().select('_id titre active')
        res.json(plays)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}



const remove = async (req, res) => {
    try {
        let play = req.profile  
        let deletedPlay = await play.remove()
        res.json(deletedPlay)
    } catch (err) {
        return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
        })
    }
}












module.exports = {
    create,
    playByID,pl,
    list,
    remove,
    playsByUser
}
