const app = require('./express');
const http = require('http').createServer(app);
const config = require("./config/config")

// Connection URL
const x = require("./helpers/initMongodb");


// Running the server
const port = 3000;



const server = http.listen(port, (err) => {
  if (err) {
    console.log(err)
  }
  console.info('Server started on port %s.', port)
})